FROM ubuntu:18.04

# Fix for issue: https://github.com/phusion/baseimage-docker/issues/58
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Fix for issue: https://github.com/phusion/baseimage-docker/issues/319
RUN apt-get update && apt-get -qq install --yes --no-install-recommends apt-utils

RUN apt-get -qq install --yes build-essential lsb-release libssl-dev libffi-dev sudo

RUN	apt-get -qq install --yes python3 && \
    apt-get -qq install --yes python3-dev && \
	apt-get -qq install --yes python3-pip && \
	apt-get -qq install --yes python3-venv

WORKDIR /myproject

COPY . .roots

RUN echo 'SHELL = /bin/sh' > Makefile
RUN echo '-include .roots/Makefile' >> Makefile

RUN make init

CMD ["/bin/bash", "-c", "make help"]
