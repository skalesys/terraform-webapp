provider "aws" {
  region = "${var.region}"
}

terraform {
  required_version = ">= 0.11.13"

  backend "s3" {}
}

data "aws_caller_identity" "current" {}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    region         = "${var.region}"
    bucket         = "sk-prd-terraform-state"
    key            = "terraform-vpc/${var.environment}/terraform.state"
    encrypt        = "true"
    dynamodb_table = "sk-prd-terraform-state-lock"
  }
}

data "terraform_remote_state" "dns" {
  backend = "s3"

  config {
    region         = "${var.region}"
    bucket         = "sk-prd-terraform-state"
    key            = "terraform-dns/${var.environment}/terraform.state"
    encrypt        = "true"
    dynamodb_table = "sk-prd-terraform-state-lock"
  }
}

locals {
  tags = "${merge(var.tags, map("Environment", "${var.environment}"))}"
}

#---
# S3
#---

#The backend generates statements (PDF format), which need to accessed from
#both the web portal and mobile application and need to be archived
#indefinitely.

module "statements_log_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"
  enabled = "${var.s3_enabled}"

  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  delimiter  = "${var.delimiter}"
  attributes = ["${var.s3_attributes}", "logs"]
  tags       = "${local.tags}"
}

resource "aws_s3_bucket" "statements_log" {
  bucket = "${module.statements_log_label.id}"
  acl    = "log-delivery-write"
}

module "statements_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"
  enabled = "${var.s3_enabled}"

  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  delimiter  = "${var.delimiter}"
  attributes = "${var.s3_attributes}"
  tags       = "${local.tags}"
}

resource "aws_s3_bucket" "statements" {
  bucket        = "${module.statements_label.id}"
  acl           = "${var.s3_acl}"
  force_destroy = "${var.s3_force_destroy}"

  versioning {
    enabled = "${var.s3_versioning_enabled}"
  }

  # https://docs.aws.amazon.com/AmazonS3/latest/dev/bucket-encryption.html
  # https://www.terraform.io/docs/providers/aws/r/s3_bucket.html#enable-default-server-side-encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "${var.s3_sse_algorithm}"
      }
    }
  }

  logging {
    target_bucket = "${aws_s3_bucket.statements_log.id}"
    target_prefix = "log/"
  }

  lifecycle_rule {
    id      = "pdf"
    enabled = true

    prefix = "pdf/"

    tags = {
      "rule"      = "pdf"
      "autoclean" = "true"
    }

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = 60
      storage_class = "ONEZONE_IA"
    }

    transition {
      days          = 90
      storage_class = "GLACIER"
    }

    expiration {
      days = 120
    }
  }

  tags = "${module.statements_label.tags}"
}

data "aws_iam_policy_document" "statements_policy" {
  statement {
    principals {
      identifiers = ["*"]
      type        = "*"
    }

    effect    = "Allow"
    actions   = ["s3:List*", "s3:Get*", "s3:Put*"]
    resources = ["${aws_s3_bucket.statements.arn}", "${aws_s3_bucket.statements.arn}/*"]

    condition {
      test     = "IpAddress"
      variable = "aws:SourceIp"
      values   = ["${data.terraform_remote_state.vpc.private_subnets_cidr_blocks}"]
    }
  }
}

resource "aws_s3_bucket_policy" "statements" {
  bucket = "${aws_s3_bucket.statements.id}"
  policy = "${data.aws_iam_policy_document.statements_policy.json}"
}

#----
# ALB
#----

module "frontend_alb" {
  source  = "git::https://github.com/skalesys/terraform-aws-alb.git"
  version = "master"

  namespace  = "${var.namespace}"
  name       = "${var.name}"
  stage      = "${var.stage}"
  attributes = ["front", "alb"]

  vpc_id          = "${data.terraform_remote_state.vpc.vpc_id}"
  subnet_ids      = ["${data.terraform_remote_state.vpc.private_subnets}"]
  ip_address_type = "ipv4"
  target_type     = "instance"

  https_enabled      = "true"
  certificate_arn    = "${var.acm_certificate_arn}"
  access_logs_region = "${var.region}"

  tags = "${local.tags}"
}

module "backend_alb" {
  source  = "git::https://github.com/skalesys/terraform-aws-alb.git"
  version = "master"

  namespace  = "${var.namespace}"
  name       = "${var.name}"
  stage      = "${var.stage}"
  attributes = ["back", "alb"]

  vpc_id          = "${data.terraform_remote_state.vpc.vpc_id}"
  subnet_ids      = ["${data.terraform_remote_state.vpc.private_subnets}"]
  ip_address_type = "ipv4"
  target_type     = "instance"

  https_enabled      = "true"
  certificate_arn    = "${var.acm_certificate_arn}"
  access_logs_region = "${var.region}"

  tags = "${local.tags}"
}

#--------
# BASTION
#--------

module "bastion_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["bastion"]
  delimiter  = "-"
  tags       = "${local.tags}"
}

module "bastion_sg_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["bastion", "sg"]
  delimiter  = "-"
  tags       = "${local.tags}"
}

module "bastion_private_sg_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["bastion", "private", "sg"]
  delimiter  = "-"
  tags       = "${local.tags}"
}

resource "aws_security_group" "bastion" {
  name_prefix = "${module.bastion_sg_label.id}-"
  description = "Bastion security group"
  vpc_id      = "${data.terraform_remote_state.vpc.vpc_id}"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = ["${var.bastion_allowed_cidr_blocks}"]
    description = "Allow SSH connections"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all outgoing connections"
  }

  lifecycle {
    create_before_destroy = "true"
  }

  tags = "${module.bastion_sg_label.tags}"
}

resource "aws_security_group" "bastion_private" {
  name_prefix = "${module.bastion_private_sg_label.id}-"
  description = "Bastion security group for private subnets"
  vpc_id      = "${data.terraform_remote_state.vpc.vpc_id}"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = ["${aws_instance.bastion.private_ip}/32"]
    description = "Allow SSH connections"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all outgoing connections"
  }

  lifecycle {
    create_before_destroy = "true"
  }

  tags = "${module.bastion_private_sg_label.tags}"

  depends_on = ["aws_instance.bastion"]
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "bastion" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"

  vpc_security_group_ids = ["${aws_security_group.bastion.id}"]
  subnet_id              = "${data.terraform_remote_state.vpc.public_subnets[0]}"
  key_name               = "${module.ssh_key_pair.key_name}"

  tags        = "${module.bastion_label.tags}"
  volume_tags = "${merge(module.bastion_label.tags, map("Snapshot", "true"))}"

  credit_specification {
    cpu_credits = "standard"
  }

  depends_on = ["aws_security_group.bastion"]
}

module "bastion_eip_label" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git"
  version    = "0.7.0"
  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["bastion", "eip"]
  delimiter  = "-"
  tags       = "${local.tags}"
}

resource "aws_eip" "bastion" {
  instance = "${aws_instance.bastion.id}"
  tags     = "${module.bastion_eip_label.tags}"
  vpc      = true
}

#----
# EC2
#----

module "ssh_key_pair" {
  source  = "git::https://github.com/cloudposse/terraform-aws-key-pair.git"
  version = "0.3.2"

  namespace = "${var.namespace}"
  stage     = "${var.stage}"
  name      = "${var.name}"

  ssh_public_key_path   = "secrets/"
  generate_ssh_key      = "true"
  private_key_extension = ".pem"
  public_key_extension  = ".pub"
  chmod_command         = "chmod 600 %v"
}

# Uploads the key pair to S3 bucket
variable "backup_ssh_key_pair" {
  default = "false"
}

resource "aws_s3_bucket_object" "ssh_key_private_key" {
  count  = "${var.backup_ssh_key_pair == "true" ? 1 : 0}"
  bucket = "skalesys-bucket"
  key    = "terraform/webapp/${module.ssh_key_pair.private_key_filename}"
  source = "${module.ssh_key_pair.private_key_filename}"
  etag   = "${filemd5("${module.ssh_key_pair.private_key_filename}")}"
}

resource "aws_s3_bucket_object" "ssh_key_public_key" {
  count  = "${var.backup_ssh_key_pair == "true" ? 1 : 0}"
  bucket = "skalesys-bucket"
  key    = "terraform/webapp/${module.ssh_key_pair.public_key_filename}"
  source = "${module.ssh_key_pair.public_key_filename}"
  etag   = "${filemd5("${module.ssh_key_pair.public_key_filename}")}"
}

data "aws_iam_policy_document" "ec2_assume_role" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      identifiers = [
        "ec2.amazonaws.com",
      ]

      type = "Service"
    }

    effect = "Allow"
  }
}

data "aws_iam_policy_document" "read_put_statements" {
  statement {
    effect    = "Allow"
    actions   = ["s3:List*", "s3:Get*", "s3:Put*"]
    resources = ["${aws_s3_bucket.statements.arn}", "${aws_s3_bucket.statements.arn}/*"]
  }
}

resource "aws_iam_policy" "read_put_statements_policy" {
  name        = "test-policy"
  description = "A test policy"

  policy = "${data.aws_iam_policy_document.read_put_statements.json}"
}

module "iam_role_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["iam", "assume", "role", "ec2"]
  delimiter  = "-"
  tags       = "${local.tags}"
}

resource "aws_iam_role" "role" {
  name = "${module.iam_role_label.id}"
  path = "/service/"

  assume_role_policy = "${data.aws_iam_policy_document.ec2_assume_role.json}"
}

resource "aws_iam_role_policy_attachment" "read_put_statement_policy_attach" {
  role       = "${aws_iam_role.role.name}"
  policy_arn = "${aws_iam_policy.read_put_statements_policy.arn}"
}

module "instance_profile_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"

  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["iam", "instance", "profile"]
  delimiter  = "-"
  tags       = "${local.tags}"
}

resource "aws_iam_instance_profile" "profile" {
  name = "${module.instance_profile_label.id}"
  role = "${aws_iam_role.role.name}"
}

module "frontend_asg_group" {
  source  = "git::https://github.com/cloudposse/terraform-aws-ec2-autoscale-group.git"
  version = "0.1.3"

  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["frontend", "asg"]

  image_id                  = "${data.aws_ami.ubuntu.id}"
  key_name                  = "${module.ssh_key_pair.key_name}"
  instance_type             = "${var.frontend_asg_instance_type}"
  security_group_ids        = ["${module.frontend_alb.security_group_id}", "${aws_security_group.bastion_private.id}", "${aws_security_group.database.id}"]
  iam_instance_profile_name = "${aws_iam_instance_profile.profile.name}"

  subnet_ids                  = ["${data.terraform_remote_state.vpc.private_subnets}"]
  health_check_type           = "EC2"
  min_size                    = "${var.frontend_asg_min_size}"
  max_size                    = "${var.frontend_asg_max_size}"
  wait_for_capacity_timeout   = "${var.frontend_asg_wait_for_capacity_timeout}"
  associate_public_ip_address = false

  # an example, to make our frontend instance healthy
  user_data_base64 = "${base64encode(file("user_data_frontend.sh"))}"

  target_group_arns = ["${module.frontend_alb.default_target_group_arn}"]

  # Auto-scaling policies and CloudWatch metric alarms
  autoscaling_policies_enabled           = "${var.frontend_asg_autoscaling_policies_enabled}"
  cpu_utilization_high_threshold_percent = "${var.frontend_asg_cpu_utilization_high_threshold_percent}"
  cpu_utilization_low_threshold_percent  = "${var.frontend_asg_cpu_utilization_low_threshold_percent}"
}

module "backend_asg_group" {
  source  = "git::https://github.com/cloudposse/terraform-aws-ec2-autoscale-group.git"
  version = "0.1.3"

  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["backend", "asg"]

  image_id                  = "${data.aws_ami.ubuntu.id}"
  key_name                  = "${module.ssh_key_pair.key_name}"
  instance_type             = "${var.backend_asg_instance_type}"
  security_group_ids        = ["${module.backend_alb.security_group_id}", "${aws_security_group.bastion_private.id}", "${aws_security_group.database.id}"]
  iam_instance_profile_name = "${aws_iam_instance_profile.profile.name}"

  subnet_ids                  = ["${data.terraform_remote_state.vpc.private_subnets}"]
  health_check_type           = "EC2"
  min_size                    = "${var.backend_asg_min_size}"
  max_size                    = "${var.backend_asg_max_size}"
  wait_for_capacity_timeout   = "${var.backend_asg_wait_for_capacity_timeout}"
  associate_public_ip_address = false

  # an example, to make our backend instance healthy
  user_data_base64 = "${base64encode(file("user_data_backend.sh"))}"

  target_group_arns = ["${module.backend_alb.default_target_group_arn}"]

  # Auto-scaling policies and CloudWatch metric alarms
  autoscaling_policies_enabled           = "${var.backend_asg_autoscaling_policies_enabled}"
  cpu_utilization_high_threshold_percent = "${var.backend_asg_cpu_utilization_high_threshold_percent}"
  cpu_utilization_low_threshold_percent  = "${var.backend_asg_cpu_utilization_low_threshold_percent}"
}

#---------
# DATABASE
#---------

# The database schema is fairly simple but will hold millions of transactional
# entries related to the customers rewards earnings and spends. The team is
# currently using MySQL locally to mock this out

module "db_sg_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"
  enabled = "${var.db_cluster}"

  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["db", "sg"]
  delimiter  = "-"
  tags       = "${local.tags}"
}

module "db_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git"
  version = "0.7.0"
  enabled = "${var.db_cluster}"

  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["db"]
  delimiter  = "-"
  tags       = "${local.tags}"
}

resource "aws_security_group" "database" {
  name_prefix = "${module.db_sg_label.id}-"
  description = "RDS security group"
  vpc_id      = "${data.terraform_remote_state.vpc.vpc_id}"

  ingress {
    from_port = "${var.db_port}"
    to_port   = "${var.db_port}"
    protocol  = "tcp"

    cidr_blocks = ["${data.terraform_remote_state.vpc.private_subnets_cidr_blocks}"]
    description = "Allow connections to database ${module.db_label.name}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all outgoing connections"
  }

  lifecycle {
    create_before_destroy = "true"
  }

  tags = "${module.db_sg_label.tags}"
}

module "db_cluster" {
  source  = "git::https://github.com/cloudposse/terraform-aws-rds-cluster.git"
  version = "0.15.0"
  enabled = "${var.db_cluster}"

  name       = "${var.name}"
  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  attributes = ["db"]

  engine            = "aurora-mysql"
  cluster_family    = "aurora-mysql5.7"
  cluster_size      = "${var.db_cluster_size}"
  storage_encrypted = "${var.db_storage_encrypted}"
  instance_type     = "${var.db_instance_type}"

  admin_user     = "${var.db_admin_user}"
  admin_password = "${var.db_admin_password}"

  db_name = "${module.db_label.name}"
  db_port = "${var.db_port}"

  zone_id             = "ZTX3RDJ8XMVZ9"
  vpc_id              = "${data.terraform_remote_state.vpc.vpc_id}"
  allowed_cidr_blocks = "${data.terraform_remote_state.vpc.private_subnets_cidr_blocks}"
  subnets             = ["${data.terraform_remote_state.vpc.database_subnets}"]
  security_groups     = ["${aws_security_group.database.id}"]

  tags = "${module.db_label.tags}"
}
